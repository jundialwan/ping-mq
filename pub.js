var amqp = require('amqplib')
var config = require('./config')

amqp.connect(config.connect)
.then((conn) => {
  return conn.createChannel()
  .then((ch) => {
    ch.assertExchange('EX_PING', 'fanout', {durable: false})

    setInterval(() => {
      ch.publish('EX_PING', '', new Buffer(JSON.stringify({
        action: 'ping',
        npm: '1306464114',
        ts: new Date().toLocaleString()
      })))      
    }, 5000)
  })
})
.catch(e => console.log(e))