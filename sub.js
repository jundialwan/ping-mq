var amqp = require('amqplib')
var config = require('./config')

amqp.connect(config.connect)
.then((conn) => {
  return conn.createChannel()
  .then((ch) => {
    ch.assertExchange('EX_PING', 'fanout', {durable: false})

    return ch.assertQueue('', {exclusive: true})
    .then((q) => {
      ch.bindQueue(q.queue, 'EX_PING', '')

      ch.consume(q.queue, (msg) => {
        console.log(msg.fields.content.data)
        const contentArrBuffer = Buffer.from(msg.fields.content.data)
        console.log(contentArrBuffer.toString())

        // write to database
      }, {noAck: true})
    })    
  })  
})
.catch(e => console.log(e))